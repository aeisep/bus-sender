import Vue from "vue";
import VueGeolocation from "vue-browser-geolocation";
import VueWebsocket from "vue-websocket";
import App from "./App.vue";
import "./registerServiceWorker";

Vue.config.productionTip = false;

Vue.use(VueGeolocation);
Vue.use(VueWebsocket, "wss://bus-server.aeisep.pt");

new Vue({
  render: h => h(App)
}).$mount("#app");
